import os.path, gym
import tensorflow as tf
import numpy as np

class PlayPolicy(object):
    def __init__(self, obs_dim, act_dim, num_hid_layers, num_hid_units, weights_file):
        """
        Builds policy network and initializes with weights in weights_file

        Args:
            obs_dim:         size of observation space
            act_dim:         size of action space
            num_hid_layers:  number of hidden layers in weights_file
            num_hid_units:   number of hidden units in weights_file
            weights_file:    policy weights trained using gym_train.py
        """
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        self.num_hid_layers = num_hid_layers
        self.num_hid_units = num_hid_units
        self.weights_file = weights_file

        self.graph = tf.Graph()
        with self.graph.as_default():
            self._tf_session()
            with tf.variable_scope('play') as scope:
                self._tf_placeholders()
                self._policy_op()
            with tf.variable_scope(scope, reuse=True):
                self._load_weights()

    def _tf_placeholders(self):
        """
        Creates graph placeholders
        """
        self.obs_ph = tf.placeholder(shape=[None, self.obs_dim], dtype=tf.float32, name="obs")

    def _policy_op(self):
        """
        Builds policy neural network
        """
        x = self.obs_ph
        for i in range(self.num_hid_layers):
            w = tf.get_variable(shape=[x.get_shape()[1], self.num_hid_units], 
                                initializer=tf.random_normal_initializer(), name='h%i_w'%(i))
            b = tf.get_variable(shape=[self.num_hid_units], 
                                initializer=tf.random_normal_initializer(), name='h%i_b'%(i))
            x = tf.nn.relu(tf.matmul(x, w) + b)

        w = tf.get_variable(shape=[x.get_shape()[1], self.act_dim], 
                            initializer=tf.random_normal_initializer(), name='out_w')
        b = tf.get_variable(shape=[self.act_dim],
                            initializer=tf.random_normal_initializer(), name='out_b')
        self.act_dist = tf.nn.softmax(tf.matmul(x, w) + b)

    def _load_weights(self):
        """
        Initializes policy network with weights in weights_file
        """
        f = open(self.weights_file, 'rb')
        weights = np.load(f)

        self.weight_nodes = []
        feed_dict = {}
        for var, w in weights.items():
            ph = tf.placeholder(tf.float32, w.shape)
            feed_dict[ph] = w
            self.weight_nodes.append(tf.assign(tf.get_variable(var), ph))

        self.sess.run(self.weight_nodes, feed_dict=feed_dict)

    def _tf_session(self):
        """
        Starts Tensorflow session
        """
        self.sess = tf.Session(graph=self.graph)
        self.sess.run(tf.global_variables_initializer())


    def act(self, obs):
        """
        Selects most probable action from distribution

        Args:
            obs: observation input to policy network

        Returns:
            act: most probable action given state under playback policy
        """
        feed_dict={self.obs_ph : [obs]}
        a_dist = self.sess.run(self.act_dist, feed_dict=feed_dict)
        act = np.argmax(a_dist)
        return act

    def end_session(self):
        """
        Closes Tensorflow session
        """
        self.sess.close()

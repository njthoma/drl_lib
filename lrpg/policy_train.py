import gym
import tensorflow as tf
import numpy as np
import pickle

class TrainPolicy(object):

    def __init__(self, obs_dim, act_dim, num_hid_layers, num_hid_units, lr):
        """
        Builds policy network and sets up training procedure

        Args:
            obs_dim:         size of observation space
            act_dim:         size of action space
            num_hid_layers:  number of hidden layers in network
            num_hid_units:   number of hidden units per layer
            lr:              learning rate for ADAM optimizer
        """
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        self.n_hid_layers = num_hid_layers
        self.n_hid_units = num_hid_units
        self.lr = lr

        self.graph = tf.Graph()
        with self.graph.as_default():
            self._tf_placeholders()
            self._policy_op()
            self._loss_op()
            self._tf_session()

    def _tf_placeholders(self):
        """
        Creates graph placeholders
        """
        self.obs_ph = tf.placeholder(shape=[None, self.obs_dim], dtype=tf.float32, name='obs')
        self.act_ph = tf.placeholder(shape=[None], dtype=tf.int32, name='act')
        self.rew_ph = tf.placeholder(shape=[None], dtype=tf.float32, name='rew')

    def _policy_op(self):
        """
        Builds policy neural network
        """
        x = self.obs_ph
        for i in range(self.n_hid_layers):
            w = tf.get_variable(shape=[x.get_shape()[1], self.n_hid_units],
                                initializer=tf.random_normal_initializer(), name='h%i_w'%(i))
            b = tf.get_variable(shape=[self.n_hid_units],
                                initializer=tf.random_normal_initializer(), name='h%i_b'%(i))
            x = tf.nn.relu(tf.matmul(x, w) + b)

        w = tf.get_variable(shape=[x.get_shape()[1], self.act_dim],
                            initializer=tf.random_normal_initializer(), name='out_w')
        b = tf.get_variable(shape=[self.act_dim],
                            initializer=tf.random_normal_initializer(), name='out_b')
        self.act_dist = tf.nn.softmax(tf.matmul(x, w) + b)

    def _loss_op(self):
        """
        Network training operation using the likelihood ratio policy gradient (lrpg)
        """
        self.act_idx = (tf.range(0, tf.shape(self.act_dist)[0]) * tf.shape(self.act_dist)[1]) + self.act_ph
        self.act_v = tf.gather(tf.reshape(self.act_dist, [-1]), self.act_idx)

        self.loss = -tf.reduce_mean(tf.log(self.act_v) * self.rew_ph)

        optimizer = tf.train.AdamOptimizer(self.lr)
        self.train = optimizer.minimize(self.loss)

    def _tf_session(self):
        """
        Starts Tensorflow session
        """
        self.sess = tf.Session(graph=self.graph)
        self.sess.run(tf.global_variables_initializer())
        self.weights = tf.trainable_variables()

    def sample(self, obs):
        """
        Selects action from distribution with some random exploration

        Args:
            obs: observation input to policy network

        Returns:
            act: action to take in environment
        """
        feed_dict={self.obs_ph : [obs]}
        dist = self.sess.run(self.act_dist, feed_dict=feed_dict)
        a = np.random.choice(dist[0],p=dist[0])
        return np.argmax(dist==a)

    def update(self, obs, act, rew):
        """
        Updates policy using likelihood ratio policy gradient
        """
        feed_dict = {self.obs_ph : obs,
                     self.act_ph : act,
                     self.rew_ph : rew}
        self.sess.run(self.train, feed_dict)

    def save(self, path):
        """
        Save policy network weights to file

        Args:
            path: path name for weights file
        """
        f = open(path, 'wb')
        weight_names = []
        for i in range(self.n_hid_layers):
            weight_names.extend(('h%i_w'%(i), 'h%i_b'%(i)))
        weight_names.extend(('out_w', 'out_b'))
        np.savez(f,**{weight_name:self.sess.run(weight) for weight_name,weight in zip(weight_names,self.weights)})
        f.close()

    def end_session(self):
        """
        Closes Tensorflow session
        """
        self.sess.close()

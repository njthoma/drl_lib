import gym, os, sys, signal
import tensorflow as tf
import numpy as np
import argparse
from policy_train import TrainPolicy
from datetime import datetime


def save_weights(env_name, policy):
    """
    Saves policy network weights to weights/'env_name'/*.weights

    Args:
        env_name: OpenAI gym environment name (default: 'CartPole-v1')
        policy:   policy network (agent)
    """
    time = datetime.now().strftime("%Y_%m_%d_%H:%M:%S.weights")
    weights_path = os.path.join('weights', env_name, time)
    if not os.path.isdir(os.path.dirname(weights_path)):
        os.makedirs(os.path.dirname(weights_path))
    print('saving weights in ' + weights_path)
    policy.save(weights_path)

class SigHandler:
    def __init__(self, save, env_name, policy):
        self.save = save
        self.env_name = env_name
        self.policy = policy
        signal.signal(signal.SIGINT, self.control_c_handler)

    def control_c_handler(self, signal, frame):
        if self.save:
            save_weights(self.env_name, self.policy)
            self.policy.end_session()
        sys.exit(0)

def init_gym(env_name):
    """
    Initialize gym environment, return dimension of observation and action spaces.

    Args:
        env_name: OpenAI gym environment name (default: 'CartPole-v1')

    Returns:
        env: OpenAI gym environment
        obs_dim: observation space dimension
        act_dim: action space dimension
    """
    env = gym.make(env_name)

    if isinstance(env.observation_space, gym.spaces.Box):
        obs_dim = env.observation_space.shape[0]
    elif isinstance(env.observation_space, gym.spaces.Discrete):
        obs_dim = env.observation_space.n
    else:
        raise ValueError("observation space must be Box or Discrete")

    if isinstance(env.action_space, gym.spaces.Box):
        act_dim = env.action_space.shape[0]
    elif isinstance(env.action_space, gym.spaces.Discrete):
        act_dim = env.action_space.n
    else:
        raise ValueError("action space must be Box or Discrete")

    return env, obs_dim, act_dim

def run_episode(env, policy, render):
    """
    Runs episode under policy

    Args:
        env:        OpenAI gym environment
        policy:     policy network (agent)
        render:     display environment during training

    Returns:
        trajectory: tensor of observation, action, and reward for each time step
    """
    obs = env.reset()
    trajectory = []

    done = False
    while not done:
        if render:
            env.render()
        act = policy.sample(obs)
        obs_1, rew, done, _ = env.step(act)
        trajectory.append([obs,act,rew])
        obs = obs_1

    return trajectory


def run_iteration(env, pi, num_episodes, render):
    """
    Collects [num_episodes] trajectories under policy pi

    Args:
        env:          OpenAI gym environment
        pi:           policy network (agent)
        num_episodes: number of trajectories to collect under policy pi
        render:       display environment during training

    Returns:
        trajectories: tensor of episodes collected by iteration
    """
    trajectories = []
    for e in range(num_episodes):
        trajectory = run_episode(env, pi, render)
        trajectories.extend(trajectory)
    return np.array(trajectories)

def reduce_trajectories(trajectories):
    """
    Separates trajectories into observations, actions, and rewards

    Args:
        trajectories:  numpy array of episode trajectories

    Returns:
        obs: observations from policy rollout
        act: actions from policy rollout
        rew: rewards from policy rollout
    """
    obs = np.vstack(trajectories[:,0])
    act = trajectories[:,1]
    rew = trajectories[:,2]
    return obs, act, rew

def discounted_reward(rew, gamma):
    """
    Discounts future reward at timestep t by factor gamma

    Args:
        rew:    rewards from policy rollout
        gamma:  reward discount factor

    Returns:
        discounted:  discounted rewards from policy rollout
    """
    discounted = np.zeros_like(rew)
    discounted_reward = 0
    for t in reversed(range(0, rew.size)):
        discounted_reward = rew[t] + (gamma * discounted_reward)
        discounted[t] = discounted_reward
    return discounted


def main(env_name, num_iterations, num_episodes, lr, num_hid_layers, num_hid_units, gamma, render, save):
    """
    Initializes environment and agent (pi), runs training iterations

    Args:
        env_name:       OpenAI gym environment name (default: 'CartPole-v1')
        render:         render environment on display during training
        num_iterations: number of training iterations
        num_episodes:   number of episodes per iteration
        lr:             learning rate for optimizer
        num_hid_layers: number of hidden layers in policy network
        num_hid_units:  number of hidden units per hidden layer
        gamma:          reward discount factor
        render:         display environment during training
        save:           save trained policy weights to file
    """
    env, obs_dim, act_dim = init_gym(env_name)
    print(obs_dim, act_dim)
    policy = TrainPolicy(obs_dim, act_dim, num_hid_layers, num_hid_units, lr)

    sig_handler = SigHandler(save, env_name, policy)

    progress = []
    for i in range(num_iterations):
        trajectories = run_iteration(env, policy, num_episodes, render)
        obs, act, rew = reduce_trajectories(trajectories)
        discounted_rew = discounted_reward(rew, gamma)
        policy.update(obs, act, discounted_rew)

        progress.append(np.sum(rew))
        if i % 100 == 0:
            print(np.mean(progress[-100:]))

    if save:
        save_weights(env_name, policy)

    policy.end_session()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-env', '--env_name', type=str, default='CartPole-v1', help='OpenAI gym environment name')
    parser.add_argument('-i', '--num_iterations', type=int, default=1000, help='number of training iterations')
    parser.add_argument('-e', '--num_episodes', type=int, default=25, help='number of episodes per iteration')
    parser.add_argument('-lr', '--lr', type=float, default=0.0001, help='learning rate')
    parser.add_argument('-hl', '--num_hid_layers', type=int, default=3, help='number of hidden layers')
    parser.add_argument('-hu', '--num_hid_units', type=int, default=40, help='number of hidden units')
    parser.add_argument('-g', '--gamma', type=float, default=0.995, help='reward discount factor')
    parser.add_argument('-r', '--render', type=bool, default=False, help='display environment during training')
    parser.add_argument('-s', '--save', type=bool, default=False, help='save policy weights')
    args = parser.parse_args()
    main(**vars(args))

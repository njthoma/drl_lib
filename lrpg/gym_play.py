import sys, signal, gym
import tensorflow as tf
import numpy as np
import argparse
from policy_play import PlayPolicy

class SigHandler:
    def __init__(self, policy):
        self.policy = policy
        signal.signal(signal.SIGINT, self.control_c_handler)

    def control_c_handler(self, signal, frame):
        self.policy.end_session()
        sys.exit(0)

def init_gym(env_name):
    """
    Initialize gym environment, return dimension of observation and action spaces.

    Args:
        env_name: OpenAI gym environment name (default: 'CartPole-v1')

    Returns:
        env: OpenAI gym environment
        obs_dim: observation space dimension
        act_dim: action space dimension
    """
    env = gym.make(env_name)

    if isinstance(env.observation_space, gym.spaces.Box):
        obs_dim = env.observation_space.shape[0]
    elif isinstance(env.observation_space, gym.spaces.Discrete):
        obs_dim = env.observation_space.n
    else:
        raise ValueError("observation space must be Box or Discrete")

    if isinstance(env.action_space, gym.spaces.Box):
        act_dim = env.action_space.shape[0]
    elif isinstance(env.action_space, gym.spaces.Discrete):
        act_dim = env.action_space.n
    else:
        raise ValueError("action space must be Box or Discrete")

    return env, obs_dim, act_dim

def run_policy(env, policy, reset):
    """
    Run trained policy in gym environment

    Args:
        env:     OpenAI gym environment
        policy:  policy trained using gym_train.py
        reset:   flag to reset environment after done
    """
    while 1:
        obs = env.reset()
        while 1:
            act = policy.act(obs)
            obs, rew, done, _ = env.step(act)
            env.render()
            if done and reset: break

def main(env_name, num_hid_layers, num_hid_units, weights_file, reset):
    """
    Initialize gym environment and policy agent, and run policy

    Args:
        env_name:         OpenAI gym environment name (default: 'CartPole-v1')
        num_hid_layers:   number of hidden layers in weights_file
        num_hid_units:    number of hidden units per layer in weights file
        weights_file:     policy weights trained using gym_train.py
        reset:            flag to reset environment after done
    """
    env, obs_dim, act_dim = init_gym(env_name)
    policy = PlayPolicy(obs_dim, act_dim, num_hid_layers, num_hid_units, weights_file)
    sig_handler = SigHandler(policy)
    run_policy(env, policy, reset)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-env', '--env_name', type=str, default='CartPole-v1', help='OpenAI gym environment name')
    parser.add_argument('-hl', '--num_hid_layers', type=int, default=3, help='number of hidden layers')
    parser.add_argument('-hu', '--num_hid_units', type=int, default=40, help='number of hidden units')
    parser.add_argument('-w', '--weights_file', type=str, default=None, help='file with saved policy params')
    parser.add_argument('-r', '--reset', type=bool, default=True, help='flag to reset environment after done')
    args = parser.parse_args()
    main(**vars(args))

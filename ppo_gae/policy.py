import gym
import tensorflow as tf
import numpy as np
import pickle
from random import shuffle

class Policy(object):
    def __init__(self, obs_dim, act_dim, name, num_hid_layers, num_hid_units):
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        self.num_hid_layers = num_hid_layers
        self.num_hid_units = num_hid_units

        with tf.variable_scope(name) as scope:
            self.scope = tf.get_variable_scope().name
            self._tf_placeholders()
            self._policy_nn()
            self._vpred_nn()
            self._logp()
            self._sample()
            self._entropy()
        self.tvars = tf.get_collection(tf.GraphKeys.GLOBAL_VARIABLES, scope=self.scope)

    def _tf_placeholders(self):
        self.obs_ph = tf.placeholder(shape=[None, self.obs_dim], dtype=tf.float32, name='obs')
        self.act_ph = tf.placeholder(shape=[None, self.act_dim], dtype=tf.float32, name='act')
        self.adv_ph = tf.placeholder(shape=[None], dtype=tf.float32, name='adv')
        self.vtarg_ph = tf.placeholder(shape=[None], dtype=tf.float32, name='vtarg')

    def _policy_nn(self):
        out = self.obs_ph
        for i in range(self.num_hid_layers):
            out_dim = out.get_shape()[1].value
            w = tf.get_variable(shape=[out_dim, self.num_hid_units], 
                                initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='h%i_w'%(i))
            b = tf.get_variable(shape=[self.num_hid_units], 
                                initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='h%i_b'%(i))
            out = tf.nn.tanh(tf.matmul(out, w) + b)

        out_dim = out.get_shape()[1].value
        w = tf.get_variable(shape=[out_dim, self.act_dim], 
                            initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='means_w')
        b = tf.get_variable(shape=[self.act_dim],
                            initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='means_b')
        self.means = tf.matmul(out, w) + b
        self.log_vars = tf.get_variable('log_vars', (self.act_dim), tf.float32,
                                   tf.constant_initializer(-1.0))

    def _vpred_nn(self):
        out = self.obs_ph
        for i in range(self.num_hid_layers):
            out_dim = out.get_shape()[1].value
            w = tf.get_variable(shape=[out_dim, self.num_hid_units],
                                initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='vpred_h%i_w'%(i))
            b = tf.get_variable(shape=[self.num_hid_units],
                                initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='vpred_h%i_b'%(i))
            out = tf.nn.tanh(tf.matmul(out, w) + b)

        out_dim = out.get_shape()[1].value
        w = tf.get_variable(shape=[out_dim, 1],
                            initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='vpred_out_w')
        b = tf.get_variable(shape=[1],
                            initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='vpred_out_b')
        self.vpred = tf.squeeze(tf.matmul(out, w) + b)

    def _logp(self):
        self.logp = -0.5 * tf.reduce_sum(tf.square(self.act_ph - self.means) / tf.exp(self.log_vars), axis=1) \
                    -0.5 * tf.reduce_sum(self.log_vars)

    def _sample(self):
        self.sample_act = self.means + tf.exp(self.log_vars / 2.0) * tf.random_normal(shape=(self.act_dim,))

    def _entropy(self):
        self.entropy = 0.5 * tf.reduce_sum(self.log_vars) \
                     + 0.5 * (np.log(2.0 * np.pi) * self.act_dim + 1)

    def get_variables(self):
        return self.tvars

import gym
import tensorflow as tf
import numpy as np
import pickle
import random
from policy import Policy

class TrainPolicy(object):
    def __init__(self, obs_dim, act_dim, num_hid_layers, num_hid_units, lr, clip, vf_coeff, ent_coeff, num_epochs, batch_size):
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        self.num_hid_layers = num_hid_layers
        self.num_hid_units = num_hid_units
        self.lr = lr
        self.lr_mult = 1.0
        self.vf_coeff = vf_coeff
        self.ent_coeff = ent_coeff
        self.clip = clip
        self.num_epochs = num_epochs
        self.batch_size = batch_size

        self.graph = tf.Graph()
        with self.graph.as_default():
            self.pi = Policy(obs_dim, act_dim, 'pi', self.num_hid_layers, self.num_hid_units)
            self.oldpi = Policy(obs_dim, act_dim, 'oldpi', self.num_hid_layers, self.num_hid_units)
            self._kl_divergence()
            self._clip_objective()
            self._tf_session()

    def _kl_divergence(self):
        kl_div = tf.reduce_sum(self.pi.log_vars) - tf.reduce_sum(self.oldpi.log_vars) \
               + tf.reduce_sum(tf.exp(self.oldpi.log_vars - self.pi.log_vars)) \
               + tf.reduce_sum(tf.square(self.pi.means - self.oldpi.means) / tf.exp(self.pi.log_vars), axis=1) \
               - self.act_dim
        self.kl_div = 0.5 * tf.reduce_mean(kl_div)

    def _clip_objective(self):
        ratio = tf.exp(self.pi.logp - self.oldpi.logp)
        clip = self.clip * self.lr_mult

        pol_loss1 = -self.pi.adv_ph * ratio
        pol_loss2 = -self.pi.adv_ph * tf.clip_by_value(ratio, 1.0-clip, 1.0+clip)
        pol_loss = tf.reduce_mean(tf.maximum(pol_loss1, pol_loss2))

        vf_loss = self.vf_coeff * tf.reduce_mean(tf.square(self.pi.vpred - self.pi.vtarg_ph))
        kl_mean = tf.reduce_mean(self.kl_div)
        ent_mean = tf.reduce_mean(self.pi.entropy)
        ent_loss = -self.ent_coeff * ent_mean

        total_loss = pol_loss + vf_loss + ent_loss
        optimizer = tf.train.AdamOptimizer(learning_rate=self.lr*self.lr_mult, epsilon=1e-5)
        self.train = optimizer.minimize(total_loss)
        self.losses = [pol_loss, vf_loss, kl_mean, ent_mean]

    def _tf_session(self):
        self.sess = tf.Session(graph=self.graph)
        self.sess.run(tf.global_variables_initializer())
        self.weights = tf.trainable_variables()

    def act(self, obs):
        feed_dict={self.pi.obs_ph : obs}
        return self.sess.run([self.pi.sample_act, self.pi.vpred], feed_dict=feed_dict)

    def assign_old_new(self):
        def zipsame(*seqs):
            L = len(seqs[0])
            assert all(len(seq) == L for seq in seqs[1:])
            return zip(*seqs)
        for (oldv, newv) in zipsame(self.oldpi.get_variables(), self.pi.get_variables()):
            self.sess.run(tf.assign(oldv, newv))

    def build_training_set(self, obs, act, adv, vtarg, binds):
        feed_dict = {self.oldpi.obs_ph : np.squeeze(obs[binds]),
                     self.oldpi.act_ph : np.squeeze(act[binds]),
                     self.pi.obs_ph    : np.squeeze(obs[binds]),
                     self.pi.act_ph    : np.squeeze(act[binds]),
                     self.pi.adv_ph    : adv[binds],
                     self.pi.vtarg_ph  : vtarg[binds]}
        return feed_dict

    def update(self, obs, act, adv, vtarg, horizon, lr_mult):
        self.lr_mult = lr_mult
        print('Training...')
        print('pol_loss | \t vf_loss | \t meankl | \t | meanent')
        inds = np.arange(horizon)
        losses = []
        for _ in range(self.num_epochs):
            random.shuffle(inds)
            for start in range(0, horizon, self.batch_size):
                end = start + self.batch_size
                binds = inds[start:end]
                feed_dict = self.build_training_set(obs, act, adv, vtarg, binds)
                l, _ = self.sess.run([self.losses, self.train], feed_dict)
                losses.append(l)
            l = np.mean(losses, axis=0)
            print(str(l[0]) + ' | \t' + str(l[1]) + ' | \t' + str(l[2]) + ' | \t' + str(l[3]))

        print('Evaluating losses...')
        losses = []
        for start in range(0, horizon, self.batch_size):
            end = start + self.batch_size
            binds = inds[start:end]
            feed_dict = self.build_training_set(obs, act, adv, vtarg, binds)
            losses.append(self.sess.run(self.losses, feed_dict))
        l = np.mean(losses, axis=0)
        print(str(l[0]) + ' | \t' + str(l[1]) + ' | \t' + str(l[2]) + ' | \t' + str(l[3]))
        return l

    def save(self, path):
        f = open(path, 'wb')
        weight_names = []
        for i in range(self.num_hid_layers):
            weight_names.extend(('h%i_w'%(i), 'h%i_b'%(i)))
        weight_names.extend(('means_w', 'means_b', 'log_vars'))
        np.savez(f,**{weight_name:self.sess.run(weight) \
                 for weight_name,weight in zip(weight_names,self.pi.get_variables())})
        f.close()

    def end_session(self):
        self.sess.close()

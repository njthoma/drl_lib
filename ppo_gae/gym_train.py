import gym, os, sys, signal
import tensorflow as tf
import numpy as np
import argparse
import random
from policy_train import TrainPolicy
from utils import Scaler
from datetime import datetime

def save_weights(env_name, policy):
    time = datetime.now().strftime("%Y_%m_%d_%H:%M:%S.weights")
    weights_path = os.path.join('weights', env_name, time)
    if not os.path.isdir(os.path.dirname(weights_path)):
        os.makedirs(os.path.dirname(weights_path))
    print('saving weights in ' + weights_path)
    policy.save(weights_path)

class SigHandler:
    def __init__(self, save, env_name, policy):
        self.save = save
        self.env_name = env_name
        self.policy = policy
        signal.signal(signal.SIGINT, self.control_c_handler)

    def control_c_handler(self, signal, frame):
        if self.save:
            save_weights(self.env_name, self.policy)
            self.policy.end_session()
        sys.exit(0)

def init_gym(env_name):
    env = gym.make(env_name)

    if isinstance(env.observation_space, gym.spaces.Box):
        obs_dim = env.observation_space.shape[0]
    elif isinstance(env.observation_space, gym.spaces.Discrete):
        obs_dim = env.observation_space.n
    else:
        raise ValueError("observation space must be Box or Discrete")

    if isinstance(env.action_space, gym.spaces.Box):
        act_dim = env.action_space.shape[0]
    elif isinstance(env.action_space, gym.spaces.Discrete):
        act_dim = env.action_space.n
    else:
        raise ValueError("action space must be Box or Discrete")

    return env, obs_dim, act_dim

def generate_trajectory(env, policy, scaler, horizon):
    t = 0
    ob = env.reset()
    new = True
    scale, offset = scaler.get()
    obs = []
    unscaled_obs = []
    actions = []
    rewards = []
    values = []
    news = []
    ep_len = 0
    ep_rew = 0
    ep_lens = []
    ep_rews = []

    while True:
        ob = ob.astype(np.float32).reshape((1, -1))
        unscaled_obs.append(ob)
        ob = (ob - offset) * scale  # center and scale observations
        ac, val = policy.act(ob)

        if t > 0 and t % horizon == 0:
            return dict(obs=obs, uobs=unscaled_obs, act=actions, rew=rewards, vpred=values,
                        new=news, nextvpred=val*(1.0-new), ep_rews=ep_rews, ep_lens=ep_lens)

        obs.append(ob)
        values.append(val)
        actions.append(ac)
        news.append(new)
        ob, rew, new, _ = env.step(ac)
        rewards.append(rew)

        ep_rew += rew
        ep_len += 1
        if new:
           ep_rews.append(ep_rew)
           ep_lens.append(ep_len)
           ep_rew = 0
           ep_len = 0
           ob = env.reset()
        t += 1

def generalized_advantage_estimate(trajectory, horizon, gamma, lam):
    new = trajectory['new']
    new.append(False)
    vpred = trajectory['vpred']
    vpred.append(trajectory['nextvpred'])
    trajectory['adv'] = gaelam = [None] * horizon
    trajectory['vtarg'] = vtarg = [None] * horizon
    rew = trajectory['rew']
    lastgaelam = 0
    for t in reversed(range(horizon)):
        nonterminal = 1 - new[t+1]
        delta = rew[t] + gamma * vpred[t+1] * nonterminal - vpred[t]
        gaelam[t] = lastgaelam = delta + gamma * lam * nonterminal * lastgaelam
        vtarg[t] = gaelam[t] + vpred[t]

def main(env_name, total_timesteps, horizon, num_epochs, batch_size, num_hid_layers, 
         num_hid_units, lr, gamma, lam, clip, vf_coeff, ent_coeff, save):
    env, obs_dim, act_dim = init_gym(env_name)
    policy = TrainPolicy(obs_dim, act_dim, num_hid_layers, num_hid_units, lr, clip, vf_coeff, ent_coeff, num_epochs, batch_size)
    scaler = Scaler(obs_dim)
    sig_handler = SigHandler(save, env_name, policy)
    generate_trajectory(env, policy, scaler, horizon)
    timesteps_so_far = 0
    total_reward, vf_loss, kl_div = [], [], []
    while timesteps_so_far < total_timesteps:
        traj = generate_trajectory(env, policy, scaler, horizon)
        generalized_advantage_estimate(traj, horizon, gamma, lam)
        scaler.update(np.array(traj['uobs']))

        obs, act, adv, vtarg = np.array(traj['obs']), np.array(traj['act']), np.array(traj['adv']), np.array(traj['vtarg'])
        adv = (adv - adv.mean()) / adv.std()
        policy.assign_old_new()

        lr_mult =  max(1.0 - float(timesteps_so_far) / total_timesteps, 0)
        losses = policy.update(obs, act, adv, vtarg, horizon, lr_mult)
        timesteps_so_far += horizon
        total_reward.append(np.mean(traj['ep_rews']))
        vf_loss.append(losses[1])
        kl_div.append(losses[2])
        print('---------------------------------')
        print('Timesteps: ' + str(timesteps_so_far))
        print('Mean Ep Len: ' + str(np.mean(traj['ep_lens'])))
        print('Mean Ep Rew: ' + str(np.mean(traj['ep_rews'])))
        print('Mean Tot Rew: ' + str(np.mean(total_reward)))
        print('Mean Val Loss: ' + str(np.mean(vf_loss)))
        print('Mean KL Div: ' + str(np.mean(kl_div)))
        print('---------------------------------')
        if timesteps_so_far % 25600 == 0:
            print('Saving...')
            save_weights(env_name, policy)
    if save:
        save_weights(env_name, policy)
    policy.end_session()

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-env', '--env_name', type=str, default='Hopper-v1',
                        help='OpenAI gym environment name')
    parser.add_argument('-t', '--total_timesteps', type=int, default=1000000,
                        help='total number of timesteps in environment')
    parser.add_argument('-tpu', '--horizon', type=int, default=2048,
                        help='number of timesteps per policy update')
    parser.add_argument('-e', '--num_epochs', type=int, default=10,
                        help='number of update epochs')
    parser.add_argument('-bs', '--batch_size', type=int, default=64,
                        help='batch size')
    parser.add_argument('-hl', '--num_hid_layers', type=int, default=2,
                        help='number of hidden layers')
    parser.add_argument('-hu', '--num_hid_units', type=int, default=64,
                        help='number of hidden units')
    parser.add_argument('-lr', '--lr', type=float, default=0.0003,
                        help='learning rate')
    parser.add_argument('-g', '--gamma', type=float, default=0.99,
                        help='reward discount factor')
    parser.add_argument('-l', '--lam', type=float, default=0.95,
                        help='generalized advantage estimate factor')
    parser.add_argument('-c', '--clip', type=float, default=0.2,
                        help='clip objective param')
    parser.add_argument('-vf', '--vf_coeff', type=float, default=0.5,
                        help='value function loss coefficient')
    parser.add_argument('-ent', '--ent_coeff', type=float, default=0.0,
                        help='entropy loss coefficient')
    parser.add_argument('-s', '--save', type=bool, default=False,
                        help='save policy weights')
    args = parser.parse_args()
    main(**vars(args))

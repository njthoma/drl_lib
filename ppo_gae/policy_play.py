import os.path, gym
import tensorflow as tf
import numpy as np

class PlayPolicy(object):
    def __init__(self, obs_dim, act_dim, num_hid_layers, num_hid_units, weights_file):
        self.obs_dim = obs_dim
        self.act_dim = act_dim
        self.num_hid_layers = num_hid_layers
        self.num_hid_units = num_hid_units
        self.weights_file = weights_file

        self.graph = tf.Graph()
        with self.graph.as_default():
            self._tf_session()
            with tf.variable_scope('pi') as scope:
                self._tf_placeholders()
                self._policy_op()
                self._sample()
            with tf.variable_scope(scope, reuse=True):
                self._load_weights()

    def _tf_placeholders(self):
        self.obs_ph = tf.placeholder(shape=[None, self.obs_dim], dtype=tf.float32, name="obs")

    def _policy_op(self):
        out = self.obs_ph
        for i in range(self.num_hid_layers):
            out_dim = out.get_shape()[1].value
            w = tf.get_variable(shape=[out_dim, self.num_hid_units], 
                                initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='h%i_w'%(i))
            b = tf.get_variable(shape=[self.num_hid_units], 
                                initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='h%i_b'%(i))
            out = tf.nn.tanh(tf.matmul(out, w) + b)

        out_dim = out.get_shape()[1].value
        w = tf.get_variable(shape=[out_dim, self.act_dim], 
                            initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='means_w')
        b = tf.get_variable(shape=[self.act_dim],
                            initializer=tf.random_normal_initializer(stddev=(1 / out_dim)), name='means_b')
        self.means = tf.matmul(out, w) + b
        self.log_vars = tf.get_variable(shape=[self.act_dim], dtype=tf.float32,
                        initializer=tf.constant_initializer(-1.0), name='log_vars')

    def _sample(self):
        self.sample_act = self.means

    def _load_weights(self):
        f = open(self.weights_file, 'rb')
        weights = np.load(f)

        self.weight_nodes = []
        feed_dict = {}
        for var, w in weights.items():
            ph = tf.placeholder(tf.float32, w.shape)
            feed_dict[ph] = w
            self.weight_nodes.append(tf.assign(tf.get_variable(var), ph))

        self.sess.run(self.weight_nodes, feed_dict=feed_dict)

    def _tf_session(self):
        self.sess = tf.Session(graph=self.graph)
        self.sess.run(tf.global_variables_initializer())


    def sample(self, obs):
        feed_dict={self.obs_ph : [obs]}
        return self.sess.run(self.sample_act, feed_dict=feed_dict)

    def end_session(self):
        self.sess.close()
